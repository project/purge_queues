<?php

namespace Drupal\purge_queues\Plugin\Purge\Queue;

use Drupal\purge\Plugin\Purge\Queue\DatabaseQueue;
use Drupal\purge\Plugin\Purge\Queue\ProxyItemInterface;

/**
 * A QueueInterface compliant database backed queue.
 *
 * @PurgeQueue(
 *   id = "database_unique_upsert",
 *    label = @Translation("Database unique (upsert)"),
 *    description = @Translation("A database backed queue that avoids duplicate items by upserting data."),
 * )
 */
class DatabaseUniqueUpsertQueue extends DatabaseQueue {

  /**
   * The database table name.
   */
  const TABLE_NAME = 'purge_queue_upsert';

  /**
   * {@inheritdoc}
   */
  public function createItem($data) {
    $hash = $this->getHash($data);

    $query = $this->connection->upsert(static::TABLE_NAME)
      ->key('item_id')
      ->fields([
        'item_id' => $this->getHash($data),
        'data' => serialize($data),
        'created' => time(),
      ]);

    if ($query->execute()) {
      return $hash;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function createItemMultiple(array $items) {
    $item_ids = $records = [];

    $time = time();
    foreach ($items as $data) {
      $hash = $this->getHash($data);
      $records[] = [
        'item_id' => $hash,
        'data' => serialize($data),
        'created' => $time,
      ];
      $item_ids[] = $hash;
    }

    $query = $this->connection
      ->upsert(static::TABLE_NAME)
      ->key('item_id')
      ->fields(['item_id', 'data', 'created']);
    foreach ($records as $record) {
      $query->values($record);
    }

    // Execute the query and return the hashes.
    if ($query->execute()) {
      return $item_ids;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * Duplicates parent::claimItem and removes casting the item_id to an integer.
   */
  public function claimItem($lease_time = 3600) {
    // Claim an item by updating its expire fields. If claim is not successful
    // another thread may have claimed the item in the meantime. Therefore loop
    // until an item is successfully claimed or we are reasonably sure there
    // are no unclaimed items left.
    while (TRUE) {
      $conditions = [':now' => time()];
      $item = $this->connection->queryRange('SELECT * FROM {' . static::TABLE_NAME . '} q WHERE ((expire = 0) OR (:now > expire)) ORDER BY created, item_id ASC', 0, 1, $conditions)->fetchObject();
      if ($item) {
        // Try to update the item. Only one thread can succeed in UPDATEing the
        // same row. We cannot rely on REQUEST_TIME because items might be
        // claimed by a single consumer which runs longer than 1 second. If we
        // continue to use REQUEST_TIME instead of the current time(), we steal
        // time from the lease, and will tend to reset items before the lease
        // should really expire.
        $update = $this->connection->update(static::TABLE_NAME)
          ->fields([
            'expire' => time() + $lease_time,
          ])
          ->condition('item_id', $item->item_id);

        // If there are affected rows, this update succeeded.
        if ($update->execute()) {
          $item->data = unserialize($item->data);
          return $item;
        }
      }
      else {
        // No items currently available to claim.
        return FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   *  Duplicates parent::claimItemMultiple and removes casting the item_id to an integer.
   */
  public function claimItemMultiple($claims = 10, $lease_time = 3600) {
    $returned_items = $item_ids = [];

    // Retrieve all items in one query.
    $conditions = [':now' => time()];
    $items = $this->connection->queryRange('SELECT * FROM {' . static::TABLE_NAME . '} q WHERE ((expire = 0) OR (:now > expire)) ORDER BY created, item_id ASC', 0, $claims, $conditions);

    // Iterate all returned items and unpack them.
    foreach ($items as $item) {
      if (!$item) {
        continue;
      }
      $item_ids[] = $item->item_id;
      $item->data = unserialize($item->data);
      $returned_items[] = $item;
    }

    // Update the items (marking them claimed) in one query.
    if (count($returned_items)) {
      $this->connection->update(static::TABLE_NAME)
        ->fields([
          'expire' => time() + $lease_time,
        ])
        ->condition('item_id', $item_ids, 'IN')
        ->execute();
    }

    // Return the generated items, whether its empty or not.
    return $returned_items;
  }

  /**
   * {@inheritdoc}
   *
   * Duplicates parent::releaseItemMultiple and removes casting the item_id to an integer.
   */
  public function releaseItemMultiple(array $items) {
    // Extract item IDs and serialized data so comparing becomes easier.
    $items_data = [];
    foreach ($items as $item) {
      $items_data[$item->item_id] = serialize($item->data);
    }

    // Figure out which items have changed their data and update just those.
    $originals = $this->connection
      ->select(static::TABLE_NAME, 'q')
      ->fields('q', ['item_id', 'data'])
      ->condition('item_id', array_keys($items_data), 'IN')
      ->execute();
    foreach ($originals as $original) {
      $item_id = $original->item_id;
      if ($original->data !== $items_data[$item_id]) {
        $this->connection->update(static::TABLE_NAME)
          ->fields(['data' => $items_data[$item_id]])
          ->condition('item_id', $item_id)
          ->execute();
      }
    }

    // Update the lease time in one single query and resolve what to return.
    $update = $this->connection->update(static::TABLE_NAME)
      ->fields(['expire' => 0])
      ->condition('item_id', array_keys($items_data), 'IN')
      ->execute();
    if ($update) {
      return [];
    }
    else {
      return $items;
    }
  }

  /**
   * {@inheritdoc}
   *
   *  Duplicates parent::selectPage and removes casting the item_id to an integer.
   */
  public function selectPage($page = 1) {
    if (($page < 1) || !is_int($page)) {
      throw new \LogicException('Parameter $page has to be a positive integer.');
    }

    $items = [];
    $limit = $this->selectPageLimit();
    $resultset = $this->connection
      ->select(static::TABLE_NAME, 'q')
      ->fields('q', ['item_id', 'expire', 'data'])
      ->orderBy('q.created', 'DESC')
      ->orderBy('q.item_id', 'DESC')
      ->range((($page - 1) * $limit), $limit)
      ->execute();
    foreach ($resultset as $item) {
      if (!$item) {
        continue;
      }
      $item->data = unserialize($item->data);
      $items[] = $item;
    }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function schemaDefinition() {
    return [
      'description' => 'Stores items in queues for the purge database unique upsert queue.',
      'fields' => [
        'item_id' => [
          'type' => 'varchar_ascii',
          'length' => 64,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Primary Key: Unique item ID.',
        ],
        'data' => [
          'type' => 'blob',
          'not null' => FALSE,
          'size' => 'big',
          'serialize' => TRUE,
          'description' => 'The arbitrary data for the item.',
        ],
        'expire' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Timestamp when the claim lease expires on the item.',
        ],
        'created' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Timestamp when the item was created.',
        ],
      ],
      'primary key' => ['item_id'],
      'indexes' => [
        'created' => ['created'],
        'expire' => ['expire'],
      ],
    ];
  }

  /**
   * Get the hash for invalidation data.
   *
   * @param $data
   *   Purge invalidation data.
   *
   * @return string
   *   Hash of the data.
   */
  protected function getHash($data): string {
    if (
      is_array($data) &&
      isset($data[ProxyItemInterface::DATA_INDEX_TYPE]) &&
      isset($data[ProxyItemInterface::DATA_INDEX_EXPRESSION])
    ) {
      $unique_identifier = $data[ProxyItemInterface::DATA_INDEX_TYPE] . ':' . $data[ProxyItemInterface::DATA_INDEX_EXPRESSION];
    }
    else {
      $unique_identifier = serialize($data);
    }
    return hash('sha256', $unique_identifier);
  }

}
