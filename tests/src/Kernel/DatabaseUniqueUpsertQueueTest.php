<?php

namespace Drupal\Tests\purge_queues\Kernel;

use Drupal\purge\Plugin\Purge\Queue\ProxyItemInterface;
use Drupal\Tests\purge\Kernel\Queue\PluginTestBase;

/**
 * Tests \Drupal\purge_queues\Plugin\Purge\Queue\DatabaseUniqueUpsertQueue.
 *
 * @group purge_queues
 */
class DatabaseUniqueUpsertQueueTest extends PluginTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['purge', 'purge_queues'];

  /**
   * {@inheritdoc}
   */
  protected $pluginId = 'database_unique_upsert';

  /**
   * Test adding duplicate items to the queue.
   */
  public function testAddDuplicateItems(): void {
    $this->assertNull($this->queue->deleteQueue());
    $this->assertEquals(0, $this->queue->numberOfItems());

    $items = [
      [
        ProxyItemInterface::DATA_INDEX_TYPE => 'foo',
        ProxyItemInterface::DATA_INDEX_STATES => [],
        ProxyItemInterface::DATA_INDEX_EXPRESSION => 'string',
        ProxyItemInterface::DATA_INDEX_PROPERTIES => [],
      ],
      [
        ProxyItemInterface::DATA_INDEX_TYPE => 'bar',
        ProxyItemInterface::DATA_INDEX_STATES => [],
        ProxyItemInterface::DATA_INDEX_EXPRESSION => 'StrinG with Capitalization',
        ProxyItemInterface::DATA_INDEX_PROPERTIES => [],
      ]
    ];

    $this->queue->createItemMultiple($items);
    $this->assertEquals(2, $this->queue->numberOfItems());

    $new_items = [
      // This item is already in the queue.
      [
        ProxyItemInterface::DATA_INDEX_TYPE => 'foo',
        ProxyItemInterface::DATA_INDEX_STATES => [],
        ProxyItemInterface::DATA_INDEX_EXPRESSION => 'string',
        ProxyItemInterface::DATA_INDEX_PROPERTIES => [],
      ],
      // This item is already in the queue.
      [
        ProxyItemInterface::DATA_INDEX_TYPE => 'bar',
        ProxyItemInterface::DATA_INDEX_STATES => [],
        ProxyItemInterface::DATA_INDEX_EXPRESSION => 'StrinG with Capitalization',
        ProxyItemInterface::DATA_INDEX_PROPERTIES => [],
      ],
      // This item is not in the queue.
      [
        ProxyItemInterface::DATA_INDEX_TYPE => 'url',
        ProxyItemInterface::DATA_INDEX_STATES => [],
        ProxyItemInterface::DATA_INDEX_EXPRESSION => 'https://drupal.org/project/purge_queues',
        ProxyItemInterface::DATA_INDEX_PROPERTIES => [],
      ],
    ];

    // Test that duplicates are not added via createItemMultiple.
    $this->queue->createItemMultiple($new_items);
    $this->assertEquals(3, $this->queue->numberOfItems());

    $this->queue->deleteQueue();

    foreach ($new_items as $i => $item) {
      $this->assertEquals($i, $this->queue->numberOfItems());
      // Test that duplicate is not added via createItem.
      $this->queue->createItem($item);
      $this->queue->createItem($item);
      $this->assertEquals($i + 1, $this->queue->numberOfItems());
    }
  }

  /**
   * Test creating, claiming and releasing of items.
   */
  public function testCreatingClaimingAndReleasing(): void {
    $this->queue->createItem([1, 2, 3]);
    $claim = $this->queue->claimItem(3600);
    // Change the claim data to verify that releasing changed data, persists.
    $claim->data = [4, 5, 6];
    $this->assertSame(FALSE, $this->queue->claimItem(3600));
    $this->assertSame(TRUE, $this->queue->releaseItem($claim));
    $this->assertSame(TRUE, is_object($claim = $this->queue->claimItem(3600)));
    $this->assertSame($claim->data, [4, 5, 6]);
    $this->queue->releaseItem($claim);
    $this->assertSame(
      count($this->queue->createItemMultiple([1, 2, 3, 4])),
      4
    );
    $claims = $this->queue->claimItemMultiple(5, 3600);
    foreach ($claims as $i => $claim) {
      $claim->data = 9;
      $claims[$i] = $claim;
    }
    $this->assertSame($this->queue->claimItemMultiple(5, 3600), []);
    $this->assertSame($this->queue->releaseItemMultiple($claims), []);
    $claims = $this->queue->claimItemMultiple(5, 3600);
    $this->assertSame(count($claims), 5);
    foreach ($claims as $i => $claim) {
      $this->assertEquals(9, $claim->data);
    }

    $this->queue->deleteQueue();
  }

  /**
   * Test the paging behavior.
   */
  public function testPaging(): void {
    $this->assertEquals(0, $this->queue->numberOfItems());
    // Assert that setting the paging limit, gets reflected properly.
    $this->assertEquals(15, $this->queue->selectPageLimit());
    $this->assertEquals(37, $this->queue->selectPageLimit(37));
    $this->assertEquals(37, $this->queue->selectPageLimit());
    $this->assertEquals(7, $this->queue->selectPageLimit(7));
    $this->assertEquals(7, $this->queue->selectPageLimit());
    // Assert that an empty queue, results in no pages at all.
    $this->assertEquals(0, $this->queue->selectPageMax());
    $this->assertEquals([], $this->queue->selectPage());
    // Create 25 items, which should be 3,5 (so 4) pages of 7 items each.
    $this->assertSame(
      25,
      count(
        $this->queue->createItemMultiple(
          [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25,
          ])
      )
    );
    $this->assertEquals(4, $this->queue->selectPageMax());
    $this->assertEquals(5, $this->queue->selectPageLimit(5));
    $this->assertEquals(5, $this->queue->selectPageMax());
    $this->assertEquals(7, $this->queue->selectPageLimit(7));
    // Test the data in the first page, omit the page parameter which is 1.
    $page_1 = $this->queue->selectPage();
    $this->assertEquals(7, count($page_1));
    // Test the second page, which should be 7 items.
    $page_2 = $this->queue->selectPage(2);
    $this->assertEquals(7, count($page_2));
    // Test the third page, which should be 7 items as well.
    $page_3 = $this->queue->selectPage(3);
    $this->assertEquals(7, count($page_3));
    // The last page should only be 4 items in total.
    $page_4 = $this->queue->selectPage(4);
    $this->assertEquals(4, count($page_4));
    // And obviously, there should be no fifth page.
    $this->assertEquals([], $this->queue->selectPage(5));

    $this->queue->deleteQueue();
  }

}
